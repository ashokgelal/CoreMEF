﻿#region usings

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Prismactic.Core.Extensions;
using Prismactic.Core.Logging;
using Prismactic.Core.Modularity;

#endregion

namespace Prismactic.Core.MEF.Modularity
{
    /// <summary>
    ///     Component responsible for coordinating the modules' type loading and module initialization process.
    /// </summary>
    /// <remarks>
    ///     This allows the MefBootstrapper to provide this class as a default implementation.
    ///     If another implementation is found, this export will not be used.
    /// </remarks>
    [Export(typeof (IModuleManager))]
    public class PrismacticModuleManager : ModuleManager, IPartImportsSatisfiedNotification
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PrismacticModuleManager" /> class.
        /// </summary>
        /// <param name="moduleInitializer">Service used for initialization of modules.</param>
        /// <param name="moduleCatalog">Catalog that enumerates the modules to be loaded and initialized.</param>
        /// <param name="loggerFacade">Logger used during the load and initialization of modules.</param>
        [ImportingConstructor]
        public PrismacticModuleManager(
            IModuleInitializer moduleInitializer,
            IModuleCatalog moduleCatalog,
            ILoggerFacade loggerFacade)
            : base(moduleInitializer, moduleCatalog, loggerFacade)
        {
        }


        /// <summary>
        ///     Gets or sets the modules to be imported.
        /// </summary>
        /// <remarks>Import the available modules from the MEF container</remarks>
        [ImportMany(AllowRecomposition = true)]
        protected IEnumerable<Lazy<IModule, IModuleExport>> ImportedModules { get; set; }

        /// <summary>
        ///     Called when a part's imports have been satisfied and it is safe to use.
        /// </summary>
        /// <remarks>
        ///     Whenever the MEF container loads new types that cause ImportedModules to be recomposed, this is called.
        ///     This method ensures that as the MEF container discovered new modules, the ModuleCatalog is updated.
        /// </remarks>
        public virtual void OnImportsSatisfied()
        {
            // To prevent a double foreach loop, we key on the module type for anything already in the catalog.
            IDictionary<string, ModuleInfo> registeredModules = ModuleCatalog.Modules.ToDictionary(m => m.ModuleName);

            foreach (var lazyModule in ImportedModules)
            {
                // It is important that the Metadata.ModuleType is used here. 
                // Using GetType().Name would cause the Module to be constructed here rather than lazily when the module is needed.
                var moduleType = lazyModule.Metadata.ModuleType;

                ModuleInfo registeredModule;
                if (!registeredModules.TryGetValue(lazyModule.Metadata.ModuleName, out registeredModule))
                {
                    // If the module is not already in the catalog is it added.
                    var moduleInfo = new ModuleInfo
                                     {
                                         ModuleName = lazyModule.Metadata.ModuleName,
                                         ModuleType = moduleType.AssemblyQualifiedName,
                                         State = ModuleState.ReadyForInitialization,
                                     };

                    if (lazyModule.Metadata.DependsOnModuleNames != null)
                    {
                        moduleInfo.DependsOn.AddRange(lazyModule.Metadata.DependsOnModuleNames);
                    }

                    ModuleCatalog.AddModule(moduleInfo);
                }
                else
                {
                    // If the module is already in the catalog then override the module type name from the imported module
                    registeredModule.ModuleType = moduleType.AssemblyQualifiedName;
                }
            }

            LoadModulesThatAreReadyForLoad();
        }

        /// <summary>
        ///     Checks if the module needs to be retrieved before it's initialized.
        /// </summary>
        /// <param name="moduleInfo">Module that is being checked if needs retrieval.</param>
        /// <returns>True if the module needs to be retrieved.  Otherwise, false.</returns>
        protected override bool ModuleNeedsRetrieval(ModuleInfo moduleInfo)
        {
            return ImportedModules == null
                   || ImportedModules.All(lazyModule => lazyModule.Metadata.ModuleName != moduleInfo.ModuleName);
        }

        // disable the warning that the field is never assigned to, and will always have its default value null
        // as it is imported by MEF
#pragma warning disable 0649
        [Import(AllowRecomposition = false)] private PrismacticFileModuleTypeLoader _prismacticFileModuleTypeLoader;
#pragma warning restore 0649

        private IEnumerable<IModuleTypeLoader> mefTypeLoaders;

        /// <summary>
        ///     Gets or sets the type loaders used by the module manager.
        /// </summary>
        public override IEnumerable<IModuleTypeLoader> ModuleTypeLoaders
        {
            get
            {
                return mefTypeLoaders ?? (mefTypeLoaders = new List<IModuleTypeLoader> {_prismacticFileModuleTypeLoader});
            }

            set { mefTypeLoaders = value; }
        }
    }
}