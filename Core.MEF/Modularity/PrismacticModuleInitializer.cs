﻿#region usings

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Prismactic.Core.Extensions;
using Prismactic.Core.Logging;
using Prismactic.Core.MEF.Bootstrapper;
using Prismactic.Core.MEF.Properties;
using Prismactic.Core.Modularity;
using Prismactic.Core.Properties;

#endregion

namespace Prismactic.Core.MEF.Modularity
{
    /// <summary>
    ///     Exports the ModuleInitializer using the Managed Extensibility Framework (MEF).
    /// </summary>
    /// <remarks>
    ///     This allows the <see cref="PrismacticBootstrapper" /> to provide this class as a default implementation.
    ///     If another implementation is found, this export will not be used.
    /// </remarks>
    [Export(typeof (IModuleInitializer))]
    public class PrismacticModuleInitializer : ModuleInitializer
    {
        // disable the warning that the field is never assigned to, and will always have its default value null
        // as it is imported by MEF
#pragma warning disable 0649

        /// <summary>
        ///     Import the available modules from the MEF container
        /// </summary>
        [ImportMany(AllowRecomposition = true)]
        private IEnumerable<Lazy<IModule, IModuleExport>> ImportedModules { get; [UsedImplicitly] set; }
#pragma warning restore 0649

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrismacticModuleInitializer" /> class.
        /// </summary>
        /// <param name="serviceLocator">The container that will be used to resolve the modules by specifying its type.</param>
        /// <param name="loggerFacade">The logger to use.</param>
        [ImportingConstructor]
        public PrismacticModuleInitializer(IServiceLocator serviceLocator, ILoggerFacade loggerFacade)
            : base(serviceLocator, loggerFacade)
        {
        }

        /// <summary>
        ///     Uses the container to resolve a new <see cref="IModule" /> by specifying its <see cref="Type" />.
        /// </summary>
        /// <param name="moduleInfo">The module to create.</param>
        /// <returns>
        ///     A new instance of the module specified by <paramref name="moduleInfo" />.
        /// </returns>
        protected override IModule CreateModule(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");
            if (ImportedModules != null && ImportedModules.Count() != 0)
            {
                var lazyModule =
                    ImportedModules.FirstOrDefault(x => (x.Metadata.ModuleName == moduleInfo.ModuleName));
                if (lazyModule != null)
                {
                    return lazyModule.Value;
                }
            }

            // This does not fall back to the base implementation because the type must be in the MEF container and not just in the application domain.
            throw new ModuleInitializeException(
                string.Format(CultureInfo.CurrentCulture, Resources.FailedToGetType, moduleInfo.ModuleType));
        }
    }
}