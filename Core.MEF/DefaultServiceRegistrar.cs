﻿#region usings

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Reflection;
using Prismactic.Core.MEF.Bootstrapper;

#endregion

namespace Prismactic.Core.MEF
{
    /// <summary>
    ///     DefaultPrismServiceRegistrationAgent allows the Prismactic required types to be registered if necessary.
    /// </summary>
    public static class DefaultServiceRegistrar
    {
        /// <summary>
        ///     Registers the required Prism types that are not already registered in the <see cref="AggregateCatalog" />.
        /// </summary>
        /// <param name="aggregateCatalog">
        ///     The <see cref="AggregateCatalog" /> to register the required types in, if they are not
        ///     already registered.
        /// </param>
        public static AggregateCatalog RegisterRequiredPrismServicesIfMissing(AggregateCatalog aggregateCatalog)
        {
            if (aggregateCatalog == null) throw new ArgumentNullException("aggregateCatalog");
            var partsToRegister =
                GetRequiredPrismPartsToRegister(aggregateCatalog);

            var cat = new PrismacticDefaultsCatalog(partsToRegister);
            aggregateCatalog.Catalogs.Add(cat);
            return aggregateCatalog;
        }

        private static IEnumerable<ComposablePartDefinition> GetRequiredPrismPartsToRegister(ComposablePartCatalog aggregateCatalog)
        {
            var partsToRegister = new List<ComposablePartDefinition>();
            var catalogWithDefaults = GetDefaultComposablePartCatalog();
            foreach (var part in catalogWithDefaults.Parts)
            {
                foreach (var export in part.ExportDefinitions)
                {
                    var exportAlreadyRegistered = false;
                    foreach (var registeredPart in aggregateCatalog.Parts)
                    {
                        if (
                            registeredPart.ExportDefinitions.Any(
                                registeredExport =>
                                    string.Compare(registeredExport.ContractName, export.ContractName,
                                        StringComparison.Ordinal) == 0))
                        {
                            exportAlreadyRegistered = true;
                        }
                    }

                    if (exportAlreadyRegistered) continue;
                    if (!partsToRegister.Contains(part))
                    {
                        partsToRegister.Add(part);
                    }
                }
            }
            return partsToRegister;
        }

        /// <summary>
        ///     Returns an <see cref="AssemblyCatalog" /> for the current assembly
        /// </summary>
        /// <returns>
        ///     Returns an <see cref="AssemblyCatalog" /> for the current assembly
        /// </returns>
        private static ComposablePartCatalog GetDefaultComposablePartCatalog()
        {
            return new AssemblyCatalog(Assembly.GetAssembly(typeof (PrismacticBootstrapper)));
        }
    }
}