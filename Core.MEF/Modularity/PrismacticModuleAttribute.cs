﻿#region usings

using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using Prismactic.Core.Extensions;
using Prismactic.Core.Modularity;

#endregion

namespace Prismactic.Core.MEF.Modularity
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class PrismacticModuleAttribute : ExportAttribute, IModuleExport
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PrismacticModuleAttribute" /> class.
        /// </summary>
        /// <param name="moduleName">The contract name of the module.</param>
        /// <param name="moduleType">The concrete type of the module being exported. Not typeof(IModule).</param>
        public PrismacticModuleAttribute(Type moduleType, string moduleName = null)
            : base(typeof (IModuleExport))
        {
            moduleType.EnsureParameterNotNull("moduleType");
            ModuleType = moduleType;
            if (string.IsNullOrWhiteSpace(moduleName))
                moduleName = ModuleType.Name;
            ModuleName = moduleName;
        }

        public string Guid { get; set; }

        /// <summary>
        ///     Gets the contract name of the module.
        /// </summary>
        public string ModuleName { get; private set; }

        /// <summary>
        ///     Gets concrete type of the module being exported. Not typeof(IModule).
        /// </summary>
        public Type ModuleType { get; private set; }

        /// <summary>
        ///     Gets or sets the contract names of modules this module depends upon.
        /// </summary>
        [DefaultValue(new string[0])]
        public string[] DependsOnModuleNames { get; set; }
    }
}