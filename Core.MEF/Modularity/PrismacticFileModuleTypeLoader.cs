#region usings

using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using Prismactic.Core.Extensions;
using Prismactic.Core.Modularity;

#endregion

namespace Prismactic.Core.MEF.Modularity
{
    /// <summary>
    ///     Loads modules from an arbitrary location on the filesystem. This typeloader is only called if
    ///     <see cref="ModuleInfo" /> classes have a Ref parameter that starts with "file://".
    ///     This class is only used on the Desktop version of the Composite Application Library when used with Managed
    ///     Extensibility Framework.
    /// </summary>
    [Export]
    public class PrismacticFileModuleTypeLoader : FileModuleTypeLoader
    {
        private const string REF_FILE_PREFIX = "file://";

        // disable the warning that the field is never assigned to, and will always have its default value null
        // as it is imported by MEF
#pragma warning disable 0649
        [Import(AllowRecomposition = false)] private AggregateCatalog aggregateCatalog;
#pragma warning restore 0649

        /// <summary>
        ///     Retrieves the <paramref name="moduleInfo" />.
        /// </summary>
        /// <param name="moduleInfo">Module that should have it's type loaded.</param>
        public override void LoadModuleType(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");

            try
            {
                var uri = new Uri(moduleInfo.Ref, UriKind.RelativeOrAbsolute);

                // If this module has already been downloaded, I fire the completed event.
                if (IsSuccessfullyDownloaded(uri))
                {
                    RaiseLoadModuleCompleted(moduleInfo, null);
                }
                else
                {
                    var path = moduleInfo.Ref.StartsWith(REF_FILE_PREFIX + "/", StringComparison.Ordinal)
                        ? moduleInfo.Ref.Substring(REF_FILE_PREFIX.Length + 1)
                        : moduleInfo.Ref.Substring(REF_FILE_PREFIX.Length);

                    aggregateCatalog.Catalogs.Add(new AssemblyCatalog(path));
                    RecordDownloadSuccess(uri);
                    RaiseLoadModuleCompleted(moduleInfo, null);
                }
            }
            catch (Exception ex)
            {
                RaiseLoadModuleCompleted(moduleInfo, ex);
            }
        }
    }
}