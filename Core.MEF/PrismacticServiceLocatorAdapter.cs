#region usings

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.Practices.ServiceLocation;

#endregion usings

namespace Prismactic.Core.MEF
{
    /// <summary>
    ///     Provides service location utilizing the Managed Extensibility Framework container.
    /// </summary>
    public class PrismacticServiceLocatorAdapter : ServiceLocatorImplBase
    {
        private readonly CompositionContainer _compositionContainer;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrismacticServiceLocatorAdapter" /> class.
        /// </summary>
        /// <param name="compositionContainer">The MEF composition container.</param>
        public PrismacticServiceLocatorAdapter(CompositionContainer compositionContainer)
        {
            _compositionContainer = compositionContainer;
        }

        /// <summary>
        ///     Resolves the instance of the requested service.
        /// </summary>
        /// <param name="serviceType">Type of instance requested.</param>
        /// <returns>The requested service instance.</returns>
        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            var instances = new List<object>();

            var exports = _compositionContainer.GetExports(serviceType, null, null);
            instances.AddRange(exports.Select(export => export.Value));

            return instances;
        }

        /// <summary>
        ///     Resolves all the instances of the requested service.
        /// </summary>
        /// <param name="serviceType">Type of service requested.</param>
        /// <param name="key">Name of registered service you want. May be null.</param>
        /// <returns>Sequence of service instance objects.</returns>
        protected override object DoGetInstance(Type serviceType, string key)
        {
            var exports = _compositionContainer.GetExports(serviceType, null, key);
            var enumerable = exports as IList<Lazy<object, object>> ?? exports.ToList();
            if (enumerable.Any())
            {
                // If there is more than one value, this will throw an InvalidOperationException,
                // which will be wrapped by the base class as an ActivationException.
                return enumerable.Single().Value;
            }

            throw new ActivationException(
                FormatActivationExceptionMessage(new CompositionException("Export not found"), serviceType, key));
        }
    }
}