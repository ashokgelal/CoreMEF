﻿#region usings

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using Prismactic.Core.Extensions;

#endregion

namespace Prismactic.Core.MEF
{
    /// <summary>
    ///     A very simple custom <see href="ComposablePartCatalog" /> that takes an enumeration
    ///     of parts and returns them when requested.
    /// </summary>
    internal class PrismacticDefaultsCatalog : ComposablePartCatalog
    {
        private readonly IEnumerable<ComposablePartDefinition> _parts;

        /// <summary>
        ///     Creates a PrismacticDefaultsCatalog that will return the provided parts when requested.
        /// </summary>
        /// <param name="parts">Parts to add to the catalog</param>
        /// <exception cref="ArgumentNullException">Thrown if the parts parameter is null.</exception>
        public PrismacticDefaultsCatalog(IEnumerable<ComposablePartDefinition> parts)
        {
            parts.EnsureParameterNotNull("parts");
            _parts = parts;
        }

        /// <summary>
        ///     Gets the parts contained in the catalog.
        /// </summary>
        public override IQueryable<ComposablePartDefinition> Parts
        {
            get { return _parts.AsQueryable(); }
        }
    }
}