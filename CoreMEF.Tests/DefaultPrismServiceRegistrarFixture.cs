﻿#region usings

using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using Microsoft.Practices.ServiceLocation;
using NSubstitute;
using NUnit.Framework;
using Prismactic.Core.Logging;
using Prismactic.Core.MEF;
using Prismactic.Core.Modularity;
using Prismactic.Events;

#endregion

namespace CoreMEF.Tests
{
    [TestFixture]
    public class DefaultServiceRegistrarFixture
    {
        private void DumpExportsFromCompositionContainer(CompositionContainer container)
        {
            foreach (var part in container.Catalog.Parts)
            {
                foreach (var export in part.ExportDefinitions)
                {
                    Debug.WriteLine(string.Format("Part contract name => '{0}'", export.ContractName));
                }
            }
        }

        private void SetupServiceLocator(CompositionContainer container)
        {
            container.ComposeExportedValue<IServiceLocator>(new PrismacticServiceLocatorAdapter(container));
            var serviceLocator = container.GetExportedValue<IServiceLocator>();
            ServiceLocator.SetLocatorProvider(() => serviceLocator);
        }

        private static void SetupLoggerForTest(CompositionContainer container)
        {
            container.ComposeExportedValue(Substitute.For<ILoggerFacade>());
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void NullAggregateCatalogThrows()
        {
            DefaultServiceRegistrar.RegisterRequiredPrismServicesIfMissing(null);
        }

        [Test]
        public void SingleIEventAggregatorIsRegisteredWithContainer()
        {
            var catalog = new AggregateCatalog();
            var newCatalog = DefaultServiceRegistrar.RegisterRequiredPrismServicesIfMissing(catalog);

            var container = new CompositionContainer(newCatalog);

            SetupServiceLocator(container);

            var exportedValue = container.GetExportedValue<IEventAggregator>();

            Assert.IsNotNull(exportedValue);
        }

        [Test]
        public void SingleIModuleInitializerIsRegisteredWithContainer()
        {
            var catalog = new AggregateCatalog();
            var newCatalog = DefaultServiceRegistrar.RegisterRequiredPrismServicesIfMissing(catalog);

            var container = new CompositionContainer(newCatalog);
            container.ComposeExportedValue(catalog);

            SetupLoggerForTest(container);
            SetupServiceLocator(container);

            var exportedValue = container.GetExportedValue<IModuleInitializer>();

            Assert.IsNotNull(exportedValue);
        }

        [Test]
        public void SingleIModuleManagerIsRegisteredWithContainer()
        {
            var catalog = new AggregateCatalog();
            var newCatalog = DefaultServiceRegistrar.RegisterRequiredPrismServicesIfMissing(catalog);

            var container = new CompositionContainer(newCatalog);
            container.ComposeExportedValue<IModuleCatalog>(new ModuleCatalog());

            container.ComposeExportedValue(catalog);
            SetupLoggerForTest(container);
            SetupServiceLocator(container);

            DumpExportsFromCompositionContainer(container);

            var exportedValue = container.GetExportedValue<IModuleManager>();

            Assert.IsNotNull(exportedValue);
        }
    }
}