﻿#region usings

using System.ComponentModel.Composition;
using Prismactic.Events;

#endregion

namespace Prismactic.Core.MEF.Events
{
    /// <summary>
    ///     Exports the EventAggregator using the Managed Extensibility Framework (MEF).
    /// </summary>
    /// <remarks>
    ///     This allows the MefBootstrapper to provide this class as a default implementation.
    ///     If another implementation is found, this export will not be used.
    /// </remarks>
    [Export(typeof (IEventAggregator))]
    public class PrismacticEventAggregator : EventAggregator
    {
    }
}