﻿#region usings

using Microsoft.Practices.ServiceLocation;
using Prismactic.Core.Bootstrapper;
using Prismactic.Core.Modularity;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

#endregion usings

namespace Prismactic.Core.MEF.Bootstrapper
{
    /// <summary>
    ///     Base class that provides a basic bootstrapping sequence that registers most of the Composite Application Library
    ///     assets in a MEF <see cref="CompositionContainer" />.
    /// </summary>
    /// <remarks>
    ///     This class must be overriden to provide application specific configuration.
    /// </remarks>
    public abstract class PrismacticBootstrapper : PrismacticCoreBootstrapper
    {
        /// <summary>
        ///     Gets or sets the default <see cref="AggregateCatalog" /> for the application.
        /// </summary>
        /// <value>The default <see cref="AggregateCatalog" /> instance.</value>
        protected AggregateCatalog AggregateCatalog { get; set; }

        /// <summary>
        ///     Gets or sets the default <see cref="CompositionContainer" /> for the application.
        /// </summary>
        /// <value>The default <see cref="CompositionContainer" /> instance.</value>
        protected CompositionContainer Container { get; set; }

        /// <summary>
        ///     Run the bootstrapper process.
        /// </summary>
        /// <param name="runWithDefaultConfiguration">
        ///     If <see langword="true" />, registers default
        ///     Composite Application Library services in the container. This is the default behavior.
        /// </param>
        public override void Run(bool runWithDefaultConfiguration)
        {
        }

        /// <summary>
        ///     Configures the <see cref="AggregateCatalog" /> used by MEF.
        /// </summary>
        /// <remarks>
        ///     The base implementation returns a new AggregateCatalog.
        /// </remarks>
        /// <returns>An <see cref="AggregateCatalog" /> to be used by the bootstrapper.</returns>
        protected virtual AggregateCatalog CreateAggregateCatalog()
        {
            return new AggregateCatalog();
        }

        /// <summary>
        ///     Configures the <see cref="AggregateCatalog" /> used by MEF.
        /// </summary>
        /// <remarks>
        ///     The base implementation does nothing.
        /// </remarks>
        protected virtual void ConfigureAggregateCatalog()
        {
        }

        /// <summary>
        ///     Creates the <see cref="CompositionContainer" /> that will be used as the default container.
        /// </summary>
        /// <returns>A new instance of <see cref="CompositionContainer" />.</returns>
        /// <remarks>
        ///     The base implementation registers a default MEF catalog of exports of key Prism types.
        ///     Exporting your own types will replace these defaults.
        /// </remarks>
        protected virtual CompositionContainer CreateContainer()
        {
            var container = new CompositionContainer(AggregateCatalog);
            return container;
        }

        /// <summary>
        ///     Configures the <see cref="CompositionContainer" />.
        ///     May be overwritten in a derived class to add specific type mappings required by the application.
        /// </summary>
        /// <remarks>
        ///     The base implementation registers all the types direct instantiated by the bootstrapper with the container.
        ///     If the method is overwritten, the new implementation should call the base class version.
        /// </remarks>
        protected virtual void ConfigureContainer()
        {
            RegisterBootstrapperProvidedTypes();
        }

        /// <summary>
        ///     Helper method for configuring the <see cref="CompositionContainer" />.
        ///     Registers defaults for all the types necessary for Prism to work, if they are not already registered.
        /// </summary>
        public virtual void RegisterDefaultTypesIfMissing()
        {
            AggregateCatalog = DefaultServiceRegistrar.RegisterRequiredPrismServicesIfMissing(AggregateCatalog);
        }

        /// <summary>
        ///     Helper method for configuring the <see cref="CompositionContainer" />.
        ///     Registers all the types direct instantiated by the bootstrapper with the container.
        /// </summary>
        protected virtual void RegisterBootstrapperProvidedTypes()
        {
            Container.ComposeExportedValue(Logger);
            Container.ComposeExportedValue(ModuleCatalog);
            Container.ComposeExportedValue<IServiceLocator>(new PrismacticServiceLocatorAdapter(Container));
            Container.ComposeExportedValue(AggregateCatalog);
        }

        protected override void ConfigureServiceLocator()
        {
            var serviceLocator = Container.GetExportedValue<IServiceLocator>();
            ServiceLocator.SetLocatorProvider(() => serviceLocator);
        }

        /// <summary>
        ///     Initializes the modules. May be overwritten in a derived class to use a custom Modules Catalog
        /// </summary>
        protected override void InitializeModules()
        {
            var manager = Container.GetExportedValue<IModuleManager>();
            manager.Run();
        }
    }
}