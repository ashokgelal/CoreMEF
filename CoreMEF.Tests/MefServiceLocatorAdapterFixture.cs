﻿#region usings

using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Prismactic.Core.MEF;

#endregion

namespace CoreMEF.Tests
{
    [TestFixture]
    public class MefServiceLocatorAdapterFixture
    {
        [Test]
        public void ShouldForwardResolveAllToInnerContainer()
        {
            var objectOne = new object();
            var objectTwo = new object();

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeExportedValue(objectOne);
            compositionContainer.ComposeExportedValue(objectTwo);

            var containerAdapter = new PrismacticServiceLocatorAdapter(compositionContainer);
            var returnedList = containerAdapter.GetAllInstances(typeof (object)).ToList();

            Assert.AreSame(returnedList[0], objectOne);
            Assert.AreSame(returnedList[1], objectTwo);
        }

        [Test]
        public void ShouldForwardResolveToInnerContainer()
        {
            var myInstance = new object();

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeExportedValue(myInstance);

            var containerAdapter = new PrismacticServiceLocatorAdapter(compositionContainer);

            Assert.AreSame(myInstance, containerAdapter.GetInstance(typeof (object)));
        }

        [Test]
        public void ShouldForwardResolveToInnerContainerWhenKeyIsUsed()
        {
            var myInstance = new object();

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeExportedValue("key", myInstance);

            var containerAdapter = new PrismacticServiceLocatorAdapter(compositionContainer);

            Assert.AreSame(myInstance, containerAdapter.GetInstance(typeof (object), "key"));
        }

        [Test]
        [ExpectedException(typeof (ActivationException))]
        public void ShouldThrowActivationExceptionWhenMoreThanOneInstanceAvailble()
        {
            var myInstance = new object();
            var myInstance2 = new object();

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeExportedValue(myInstance);
            compositionContainer.ComposeExportedValue(myInstance2);

            var containerAdapter = new PrismacticServiceLocatorAdapter(compositionContainer);
            containerAdapter.GetInstance(typeof (object));
        }
    }
}